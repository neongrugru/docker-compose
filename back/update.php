<?php

header('Access-Control-Allow-Origin: *');

session_start();

$number = $_POST['number'];

try{
    $PDO = new PDO('mysql:host=mysql; dbname=ok', 'root', 'root');
    foreach ($PDO->query('SELECT * FROM count') as $row) {
        $new = $row['iteration'] +1;
        $req = $PDO->prepare('UPDATE count SET iteration = :number');
        $req->execute(array(
            'number' => $new
        ));
        echo $new;
    }
} catch(PDOException $e){
    echo 'Connexion impossible';
}
